#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>

#include "util.h"
#include "game.h"

int main (int argc, char **argv) {

    int port;
    int player_socket;
    char server_ip[64];
	struct sockaddr_in addr; 
    struct player player;

    parse_player_args(argc, argv, server_ip, &port, &player);

	// create socket
	if ( (player_socket= socket(AF_INET, SOCK_STREAM, 0) ) < 0 )
        error_exit("create socket");
	
	// create addr struct
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	inet_aton(server_ip, &(addr.sin_addr));

	// connect to server
	if ( connect(player_socket, (struct sockaddr*) &addr, sizeof(struct sockaddr_in)) != 0 )
        error_exit("connect to server");
	else { 
        log_msg("Connected to server", SUCCESS_FLAG);
        start_game(player_socket, &player);
    }

	close(player_socket);
	return 0;
}
