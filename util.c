#include "util.h"


void usage (const char *filename) {
    fprintf(stdout, "Usage: %s -p <port>\n", filename);
    exit(EXIT_FAILURE);
}

void usage_player(const char *filename) {
    fprintf(stdout, "Usage: %s -s <server> -p <port> -u <username>\n", filename);
    exit(EXIT_FAILURE);
}
void parse_args(int argc, char **argv, int *port) {
    int opt;
    int flags = 0;
    while ( (opt = getopt(argc, argv, "p:")) != -1 ) {
        switch (opt) {
            case 'p':
                *port = atoi(optarg);
                flags++;
                break;
            default:
                usage(argv[0]);
        }
    }
    if ( flags != 1 )
        usage(argv[0]);
}

void parse_player_args(int argc, char **argv, char *server_ip, int *port, struct player *player) {
    int opt;
    int flags = 0;
    while ( (opt = getopt(argc, argv, "p:s:u:")) != -1 ) {
        switch (opt) {
            case 's':
                strcpy(server_ip, optarg);
                flags++;
                break;
            case 'p':
                *port = atoi(optarg);
                flags++;
                break;
            case 'u':
                strcpy(player->username, optarg);
                player->score = 0;
                flags++;
                break;
            default:
                usage_player(argv[0]);
        }
    }
    if ( flags != 3 )
        usage_player(argv[0]);
   
}
void log_msg(const char *msg, int flag) {
    switch ( flag ) {
        case SUCCESS_FLAG:
            fprintf(stdout, "[+] %s\n", msg);
            break;
        case INFO_FLAG:
            fprintf(stdout, "[*] %s\n", msg);
            break;
        case ERROR_FLAG:
            fprintf(stdout, "[-] Failed to %s\n", msg);
            break;
        default:
            puts("");
    }
}

void error_exit(const char *msg) {
   log_msg(msg, ERROR_FLAG);
   fprintf(stdout, "[?] Error: %s\n", strerror(errno));
   exit(EXIT_FAILURE);
}

void error_continue(const char *msg) {
   log_msg(msg, ERROR_FLAG);
   fprintf(stdout, "[?] Error: %s\n", strerror(errno));
}


