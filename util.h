#ifndef UTIL_H
#define UTIL_H
#define SUCCESS_FLAG 0
#define INFO_FLAG 1
#define ERROR_FLAG 2

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>


extern char *optarg;
extern int optind, opterr, optopt;
  
struct player {
    char username[64];
    size_t score;
};

void usage (const char *filename);
void usage_player (const char *filename);
void parse_args(int argc, char **argv, int *port);
void parse_player_args(int argc, char **argv, char *server_ip, int *port, struct player *player);
void log_msg(const char *msg, int flag);
void error_exit(const char *msg);
void error_continue(const char *msg);

#endif
