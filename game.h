#ifndef GAME_H
#define GAME_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>

#include "util.h"

#define QUIT -1
#define LOOSER 0
#define WINNER 1

#define FAIL 2
#define SUCCESS 3

#define ROCK 10
#define PAPER 11
#define SCISSOR 12


struct request_data {
    struct player player1;
    struct player player2;

    int player1_socket;
    int player2_socket;
};


void *handle_request(void *request_data);
void start_game(int socket, struct player *player);

void send_player_info(int socket, struct player *player);
void recv_player_info(int socket, struct player *opposite);

void send_choice(int socket, uint32_t choice);
uint32_t recv_result(int socket);


#endif
