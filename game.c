#include "game.h"


void *handle_request(void *request_data) {
    log_msg("Handling request ...", INFO_FLAG);

    struct request_data *data = (struct request_data*) request_data;
    struct player player1;
    struct player player2;
    int sock1 = data->player1_socket;
    int sock2 = data->player2_socket;
    uint32_t buff1;
    uint32_t buff2;

    // recv both playe info and send to each other
    recv_player_info(sock1, &player1);
    recv_player_info(sock2, &player2);
    send_player_info(sock1, &player2);
    send_player_info(sock2, &player1);

    // start handling the game
    while ( 1 ) {
        // recv choices
        buff1 = recv_result(sock1);
        buff2 = recv_result(sock2);

        // send result
        if ( (buff1-buff2) == 1 || (buff1-buff2) == 2) {
            send_choice(sock1, SUCCESS);
            send_choice(sock2, FAIL);
            player1.score++;
        } else {
            send_choice(sock1, FAIL);
            send_choice(sock2, SUCCESS);
            player2.score++;
        }
        
        // check break condition
        if ( player1.score >= 3 ) {
            send_choice(sock1, QUIT);
            send_choice(sock2, QUIT);
            send_choice(sock1, WINNER);
            send_choice(sock2, LOOSER);
            close(sock1);
            close(sock2);
            break;
        }
        if ( player2.score >= 3 ) {
            send_choice(sock1, QUIT);
            send_choice(sock2, QUIT);
            send_choice(sock1, LOOSER);
            send_choice(sock2, WINNER);
            close(sock1);
            close(sock2);
            break;
        }

    }
    fprintf(stdout, "[+] End of thread %d\n", pthread_self());
    return NULL;
}

void start_game(int socket, struct player *player) {

    uint32_t buffer;
    uint32_t result;
    struct player opposite; 

    send_player_info(socket, player);
    recv_player_info(socket, &opposite);

    while ( 1 ) {
        fprintf(stdout, "> Rock (0), Paper (1), Scissor (2)\n");
        fprintf(stdout, "Choose > ");
        fscanf(stdin, "%d", &buffer);

        send_choice(socket, buffer);
        result = recv_result(socket);

        switch ( result ) {
            case FAIL:
                fprintf(stdout, "FAILED :(\n");
                break;
            case SUCCESS:
                fprintf(stdout, "SUCCESS :)\n");
                break;
            case QUIT:
                result = recv_result(socket);
                if ( result == WINNER ) {
                    fprintf(stdout, "****** Congratualations *******\n");
                    fprintf(stdout, "You are the WINNER :)\n");
                    close(socket);
                    return;
                }
                if ( result == LOOSER ) {
                    fprintf(stdout, "Sorry you have LOST\n");
                    close(socket);
                    return;
                }
            default:
                error_exit("An error occured");
        }
    }
}

void send_player_info(int socket, struct player *player) {
    log_msg("sending player info ...", INFO_FLAG);
    struct player pl;

    strcpy(pl.username, player->username);
    pl.score = player->score;

    if (send(socket, (void*) &pl, sizeof(struct player), 0) < 0 )
        error_exit("send player info to server");
}

void recv_player_info(int socket, struct player *opposite) {
    log_msg("recv the opposite player info ...", INFO_FLAG);
    struct player op;

    if (recv(socket, (void*) &op, sizeof(struct player), 0) < 0 )
        error_exit("recv opoosite player info");
    
    strcpy(opposite->username, op.username);
    opposite->score = op.score;
}

void send_choice(int socket, uint32_t choice) {
    uint32_t buffer;
    buffer = choice;

    if ( send(socket, (void*) &buffer, sizeof(uint32_t), 0) < 0 )
        error_continue("send yoor choice to server");
}

uint32_t recv_result(int socket) {
    uint32_t buffer;

    if (recv(socket, (void*) &buffer, sizeof(uint32_t), 0) < 0)
        error_continue("recv your opponent choice");

    return buffer;
}
