CC = gcc
objects=util.o game.o
cflags=-lpthread

all: server client

server: $(objects)
	$(CC) $(cflags) -o $@ server.c $(objects) 

client: $(objects)
	$(CC) $(cflags) -o $@ client.c $(objects) 

debugclient: $(objects)
	$(CC) -g $(cflags) -o $@ client.c $(objects) 
%.o: %.c
	$(CC) $(cflags) $< -o $@ -c

clean:
	rm *.o 2&>/dev/null


