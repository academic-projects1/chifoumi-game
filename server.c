#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>

#include "util.h"
#include "game.h"
#define MAX_THREADS 10

int counter = 0;
pthread_t threads[MAX_THREADS];


#define exit exit(EXIT_FAILURE);

int main (int argc, char ** argv) {
    int server_socket;
    struct sockaddr_in server_addr;
    struct sockaddr_in client_addr1;
    struct sockaddr_in client_addr2;
    int port = 1337;

    // create socket
    if ( (server_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
        error_exit("create socket");
        
    // init server addr
    memset(&server_addr, 0, sizeof(struct sockaddr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    inet_aton("0.0.0.0", &(server_addr.sin_addr));

    // bind addr
    if ( bind(server_socket, (struct sockaddr*) &server_addr, sizeof(struct sockaddr_in)) < 0 )
        error_exit("bind addr");

    // listen for connection
    if ( listen(server_socket, 10) != 0 )
        error_exit("listen for connections");
    else
        log_msg("Listening for connections ...", SUCCESS_FLAG);

    int length1 = sizeof(client_addr1);
    int length2 = sizeof(client_addr2);
    while ( 1 ) {
        int player1_socket;
        int player2_socket;

        if ( (player1_socket=accept(server_socket, (struct sockaddr*) &client_addr1, &length1)) < 0 ) {
            error_continue("accept connection");
            continue;
        } else 
            log_msg("First playe connected", SUCCESS_FLAG);

        if ( (player2_socket=accept(server_socket, (struct sockaddr*) &client_addr2, &length2)) < 0 ) {
            error_continue("accept connection");
            continue;
        } else
            log_msg("Second player connected", SUCCESS_FLAG);

        struct request_data request_data;
        request_data.player1_socket = player1_socket;
        request_data.player2_socket = player2_socket;
        if ( pthread_create(&(threads[counter++]), NULL, handle_request, (void*) &request_data) < 0 )
            error_continue("spawn thread");

    }

    close(server_socket);
}	
